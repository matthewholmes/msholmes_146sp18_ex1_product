import java.text.NumberFormat;

public class Product {

    private String code;
    private String description;
    private double price;

    // Ex 7-1, step 5-6: add constructor parameters.
    public Product( String code, String description, double price) {
        this.code = code;
        this.description = description;
        this.price = price;
    } // end constructor Product

    // Ex 7-1, step 8: add method getPriceNumberFormat
    public String getPriceNumberFormat() {
        NumberFormat currency = NumberFormat.getNumberInstance();
        return currency.format(price);
    } // end method getPriceNumberFormat
    
    public String getPriceFormatted() {
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        return currency.format(price);
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }   
}
