public class ProductDB {

    // Ex 7-1, step 11: change method from static to regular
    public Product getProduct(String productCode) {
        // In a more realistic application, this code would
        // get the data for the product from a file or database
        // For now, this code just uses if/else statements
        // to return the correct product data

        // create the Product object
        String code = productCode;
        Product product;

        // fill the Product object with data
        // step Ex 7-1, step 6: create a new product based on given information
        // instead of having one product with different values
        if (code.equalsIgnoreCase("java")) {
            product = new Product(code, "Murach's Java Programming", 57.50);
        } else if (code.equalsIgnoreCase("jsp")) {
            product = new Product(code, "Murach's Java Servlets and JSP", 57.50);
        } else if (code.equalsIgnoreCase("mysql")) {
            product = new Product(code, "Murach's MySQL", 54.50);
        // Ex 7-1, step 3-4: add another product
        } else if (code.equalsIgnoreCase("c++")) {
            product = new Product(code, "Murach's c++ Programming", 250.00);
        } else {
            product = new Product("Unknown code", "Unknown product", 0.0);
        } // end if/else
        return product;
    } // end static method getProduct
} // end class ProductDB